#include "String.h"
#include <iostream>

String::String(const char* other) //constructor
{
	std::cout << "created contructor" << std::endl;
	size = strlen(other);
	char_arr = new char[size + 1];
	for (size_t i = 0; other[i] != '\0'; i++) {
		char_arr[i] = other[i];
	}
	char_arr[size] = '\0';
}

String::String(const String& other) //copy constructor
{
	std::cout << "copy" << std::endl;
	size = other.size;
	char_arr = new char[size + 1];
	for (size_t i = 0; other.char_arr[i] != '\0'; i++) {
		char_arr[i] = other.char_arr[i];
	}
	char_arr[size] = '\0';
}

String::String(String&& other) noexcept //movement constructor
{
	std::cout << "move" << std::endl;
	size = other.size;
	char_arr = other.char_arr;

	//other point needs to be empty for the destroy
	other.size = 0;
	other.char_arr = nullptr;
}


String::~String()
{
	std::cout << "Destroyed" << std::endl;
	delete(char_arr);
}

const size_t String::length()
{
	return size;
}

void String::clear()
{
	size = 0;
	delete(char_arr);
	char_arr = nullptr;
	char_arr = new char[size + 1];
	char_arr[size] = '\0';
}

bool operator==(const String& lhs, const String& rhs)
{
	if (lhs.size != rhs.size) {
		return false;
	}
	for (size_t i = 0; i < rhs.size; i++) {
		if (lhs.char_arr[i] != rhs.char_arr[i]) {
			return false;
		}
	}
	return true;
}
