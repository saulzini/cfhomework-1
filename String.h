#ifndef __STRING_H__
#define __STRING_H__
#include <cstring> // for strlen
class String
{
public:
	String(const char* other);
	String(const String& other);
	String(String&& other) noexcept;
	~String();
	const size_t length();
	void clear();
	friend bool operator ==(const String& lhs, const String& rhs);
private:
	char *char_arr;
	size_t size;
};

#endif // __STRING_H__