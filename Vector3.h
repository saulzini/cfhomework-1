#ifndef __VECTOR3_H__
#define __VECTOR3_H__
#include <math.h>


template <class T>
class Vector3
{
public:
	Vector3():x(0),y(0),z(0){}
	Vector3(T _x, T _y, T _z) : x(_x), y(_y), z(_z) {}
	Vector3(const Vector3 &other){
		x = other.x;
		y = other.y;
		z = other.z;
	}
	Vector3<T> operator +(const Vector3& b) { return Vector3(x + b.x, y + b.y, z + b.z); }
	Vector3<T> operator -(const Vector3& b) { return Vector3(x - b.x, y - b.y, z - b.z); }

	T Magnitude();
	const T Dot_product(const Vector3 &b);
	const T Angle_Between(Vector3 &b);
	const T Distance_to(const Vector3 &to);
	Vector3<T> Cross_product(const Vector3 &b);
	void Normalize();
	T x,y,z;
private:
	bool compare_epsilon(T A, T B);
};

template <class T>
T Vector3<T>::Magnitude()
{
	//TODO::Ask teacher if there is a better way todo this given warning arithmetic overflow
	return static_cast<T>( sqrt( x * x + y * y + z * z)  );
}

template <class T>
const T Vector3<T>::Dot_product(const Vector3& b)
{
	return
		(b.x * x) +
		(b.y * y) +
		(b.z * z);
}

template <class T>
const T Vector3<T>::Angle_Between(Vector3& b)
{
	T dot = Dot_product(b);
	T a_magnitude = Magnitude();
	T b_magnitude = b.Magnitude();
	T divisor = a_magnitude * b_magnitude;

	if (compare_epsilon(divisor, 0 ) ) {
		throw("Error division by 0");
		return 0;
	}

	return static_cast<T>( acos(dot / divisor) );
}

template <class T>
const T Vector3<T>::Distance_to(const Vector3& b)
{
	T sum_x = b.x - x;
	T sum_y = b.y - y;
	T sum_z = b.z - z;
	
	return static_cast<T>(
		sqrt(
			sum_x * sum_x + sum_y * sum_y + sum_z * sum_z
		)
	);
}


template <class T>
Vector3<T> Vector3<T>::Cross_product(const Vector3& b)
{
	return Vector3<T>(
		(y * b.z) - (z * b.y),
		(z * b.x) - (x * b.z),
		(x * b.y) - (y * b.x)
		);
}

template <class T>
void Vector3<T>::Normalize()
{
	T magnitude = Magnitude();
	if (compare_epsilon(magnitude, 0)) {
		throw("Error division by 0");
		return;
	}
	x = x / magnitude;
	y = y / magnitude;
	z = z / magnitude;
}

template<class T>
bool Vector3<T>::compare_epsilon(T A, T B)
{
	return (fabs(A - B) < 0.05);
}


#endif // __VECTOR3_H__