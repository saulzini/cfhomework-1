
#include "Vector3.h"
#include <iostream>
#include "String.h"
using namespace std;

void testVector3() {
	Vector3<float> a(2, 3, 4);
	Vector3<float> b(1, 2, 3);
	Vector3<float> c(a);
	Vector3<float> d = b + c;
	Vector3<float> e = c - b;

	cout << d.Magnitude() << endl;
	cout << d.Distance_to(a) << endl;
	cout << d.Dot_product(a) << endl;
	Vector3<float> f = d.Cross_product(a);
	cout << d.Angle_Between(a) << endl;
	d.Normalize();
}

class TEST {
public:
	TEST(const String& name) : string_name(name) {}
	TEST(String&& name) : string_name(move(name)) {}
	//TEST(String&& name) : string_name( (String&&)(name) ){}
private:
	String string_name;
};

String GetMeAString(String&& string_name) {
	//return (String &&) string_name;
	//return move(string_name);
	return string_name;
}

int main() {
	String a("hello"); //normal
	String b(a); //copy
	String c(String("hola")); //normal

	TEST testing_string("hello");//movement
	//comparison
	if (a == "hello") {
		cout << "equals" << endl;
	}
	
	String d =a;
	String e = GetMeAString("123");
	cout << "length:" << e.length() << endl;
	e.clear();
	return 0;
}